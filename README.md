# audio-amplifier
A 1-2 amp audio amplifier

[![audio-amp.png](https://i.postimg.cc/W18S6vtS/audio-amp.png)](https://postimg.cc/9D4GCsdq)

[![3d.jpg](https://i.postimg.cc/W1TXk3ph/3d.jpg)](https://postimg.cc/mtpYxTHs)

[![schematic.png](https://i.postimg.cc/CKK7cQq0/schematic.png)](https://postimg.cc/zy9TBp72)
